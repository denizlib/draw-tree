package com.denizlib;

public class PrintTree {

    public static void main(String[] args) {
        Integer size = Integer.valueOf(args[0]);
        printTree(size);
    }

    private static void printTree(Integer size) {
        for (int line = 0; line < size; line++) {
            for (int space = line; space < size; space++) {
                System.out.print("  ");
            }
            for (int star = 0; star < (line * 2 + 1); star++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
