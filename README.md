# What it does

Takes a commandline argument for the size and prints that many rows

Example:

java PrintTree 9

output:


                    * 
                  * * * 
                * * * * * 
              * * * * * * * 
            * * * * * * * * * 
          * * * * * * * * * * * 
        * * * * * * * * * * * * * 
      * * * * * * * * * * * * * * * 
    * * * * * * * * * * * * * * * * *
 
 
